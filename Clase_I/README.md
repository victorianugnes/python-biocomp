* #### Clase I. Bioinformática, la terminal de Unix y control de versiones con Git 
	* [Introducción a la Bioinformática. Desarrollo de software para ciencias biológicas.](https://docs.google.com/presentation/d/1sg0wpm9P-53uyDzV0UZRHZUmPI7DOX6ZLlJyViEV5-k/edit?usp=sharing)
	* [Unix/Linux: organización, arquitectura, sistema de archivos.](Clase_I/IntroduccionLinux.pdf) :penguin:
	* La terminal de Unix. ![](Clase_I/imgs/terminal2.png)
	* [Introducción a la terminal.](https://swcarpentry.github.io/shell-novice-es/01-intro/index.html)
		*  [Planilla de resumen de comandos básicos](Clase_I/Linux-comandos-basicos.pdf)
		* [Navegación de archivos y directorios.](https://swcarpentry.github.io/shell-novice-es/02-filedir/index.html)
		* [Trabajando con archivos y directorios.](https://swcarpentry.github.io/shell-novice-es/03-create/index.html)
		* [*Pipes* y filtros](https://swcarpentry.github.io/shell-novice-es/04-pipefilter/index.html)
		* [Bucles](https://swcarpentry.github.io/shell-novice-es/05-loop/index.html)
		* [Scripts de la terminal](https://swcarpentry.github.io/shell-novice-es/06-script/index.html)
	* Buscar y reemplazar en la terminal
		* [Grep & Find](https://swcarpentry.github.io/shell-novice-es/07-find/index.html)
		* [Sed & awk](https://gitlab.com/nmoreyra/python-in-biology/blob/master/Clase_II/sed-awk.md)
	* [Control de versiones con Git:](https://swcarpentry.github.io/git-novice-es/)
		* [Control Automatizado de Versiones](https://swcarpentry.github.io/git-novice-es/01-basics/index.html)
		* [Configurando Git](https://swcarpentry.github.io/git-novice-es/02-setup/index.html)
		* [Creando un repositorio](https://swcarpentry.github.io/git-novice-es/03-create/index.html)
		* [Rastreando Cambios](https://swcarpentry.github.io/git-novice-es/04-changes/index.html)
		* [Explorando el "History"](https://swcarpentry.github.io/git-novice-es/05-history/index.html)
		* [Ignorando cosas](https://swcarpentry.github.io/git-novice-es/06-ignore/index.html)
		* [Repositorios remotos en GitHub](https://swcarpentry.github.io/git-novice-es/07-github/index.html)
		* [Trabajos en colaboración](https://swcarpentry.github.io/git-novice-es/08-collab/index.html)
	* Primer programa.
